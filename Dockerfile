FROM python

RUN apt update
RUN apt install -y build-essential cmake libboost-graph-dev libboost-python-dev berkeley-abc
RUN pip install scikit-build cython

RUN git clone https://bitbucket.org/StefanoVt/placeandroutecpp.git && cd placeandroutecpp && python setup.py install && cd ..
RUN wget http://optimathsat.disi.unitn.it/releases/optimathsat-1.6.3/optimathsat-1.6.3-linux-64-bit.tar.gz && tar xf optimathsat-1.6.3-linux-64-bit.tar.gz && mv optimathsat-1.6.3-linux-64-bit/bin/optimathsat /bin
COPY . /code
WORKDIR /code
RUN pip install -r requirements.txt
RUN ln -s /usr/bin/berkeley-abc abc

CMD ["./bin/encode_csp.py", "--hardware=pegasus6", "data/sgen-twoinfour-s88-g4-0.bench"]
