import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="encode_scripts",
    version="0.0.0",
    author="Stefano Varotti",
    author_email="stefano.varotti@unitn.it",
    description="Scripts for SATToQUBO encoding",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/StefanoVt/encode_scripts/src",
    packages=setuptools.find_packages(exclude=["tests"]),
    entry_points={
        'console_scripts': ['encode=encode_scripts.encode:main',
                            "preencode=encode_scripts.preencode:main"],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["tech_mapping",
                      "placeandroute",
                      "gatecollector",
                      "dwave-ocean-sdk"],

)
