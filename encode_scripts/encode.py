#!/usr/bin/env python
import logging
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s %(asctime)s %(processName)s %(message)s')

import re
from collections import defaultdict
from math import ceil

from dimod import BinaryQuadraticModel
from dwave.cloud import Client
from dwave_networkx import pegasus_layout, pegasus_graph
from neal import SimulatedAnnealingSampler
from placeandroute.tilebased.heuristic import Constraint
from placeandroute.tilebased.utils import show_result


import argparse
from os import system
import tempfile
import placeandroute.tilebased.parallel as parheur
from multiprocessing import Pool
from tech_mapping.read_genlib import read_genlib
from placeandroute.read_blif import read_blif, chimera_map_constraint, blif_to_constraints
from placeandroute.tilebased.chimera_tiles import load_chimera, expand_solution as chimera_expand
from placeandroute.tilebased.pegasus_tiles import load_pegasus, expand_solution as pegasus_expand

hardware= {
    "chimera8": (8, load_chimera, chimera_expand),
    "chimera12": (12, load_chimera, chimera_expand),
    "chimera16": (16, load_chimera, chimera_expand),
    "pegasus6" : (6, load_pegasus, pegasus_expand),
    "pegasus8": (8, load_pegasus, pegasus_expand),
    "pegasus12": (12, load_pegasus, pegasus_expand),

}

parser = argparse.ArgumentParser(description='''Encode a SAT problem into a QA model.''')

parser.add_argument('problem', help=("input SAT problem as AIG file"))
parser.add_argument('library',  help="pre-encoded librari as GENLIB file")
parser.add_argument('--hardware', '-g', default="chimera16", choices=hardware.keys(),
                    help="QA hardware (default: chimera16)")
parser.add_argument('--sampler', help="run on a sampler", default="simulated", choices=["simulated", "sapi"])


#hack
def pegasus_map_constraint(pins, gate_data, ancilla_accum):
    #alpha order for old format
    pins_names = ["O", "I0", "I1", "I2", "I3", "I4", "I5", "I6"]


    #first half is top row, second half is bottom
    dim = gate_data["dims"][2]
    print(gate_data)
    allpos = list(range(1, dim*2+1))
    posmap = {x:0 for x in allpos[:dim]}
    posmap.update({y:1 for y in allpos[dim:]})
    ordered_wires = []
    ow = {}

    retg = [[], []]
    for pin, pos in zip(pins_names, gate_data["vP"][0]):
        pos = pos[0]
        assert isinstance(pos, int), ("error parsing genlib", pin, pos)
        retg[posmap[pos]].append(pins[pin])
        ow[pos] = pins[pin]
        del posmap[pos]

    #leftover posmap are ancilla
    for apos,to in posmap.items():
        ancilla_name = "anc_" + str(ancilla_accum)
        retg[to].append(ancilla_name)
        ow[apos] = ancilla_name
        ancilla_accum += 1

    for pos in range(1, dim*2+1):
        ordered_wires.append(ow[pos])


    return Constraint(retg), ordered_wires, ancilla_accum

def main(args=None):

    if args is None:
        args = parser.parse_args()

    with open(args.library, "r") as lf:
        library = read_genlib(lf)

    firstval = next(iter(library.values()))
    if "en0" not in  firstval:
        #needs backporting
        backp_library = {}
        for gate, data in library.items():
            nvars = int(data["area"]) - int(data["ancilla_used"])
            backp_library[gate] = backport_gate_data(nvars, data)
        library = backp_library

    selected_hardware  = hardware[args.hardware]

    #encode and techmap with abc
    with tempfile.TemporaryDirectory() as dir:
        abc_script = dir + "/techmap.abc"
        blif_out = dir + "/output.blif"
        with open(abc_script, "w") as abc:
            print("read_aiger {}".format(args.problem), file=abc)
            print("read_genlib {}".format(args.library), file=abc)
            print("source simplify.abc", file=abc)
            print("source convert.abc", file=abc)
            print("write_blif {}".format(blif_out), file=abc)
        system('./abc -F "{}"'.format(abc_script))
        system('cat {}'.format(blif_out))
        mapped_file = open(blif_out)
        blif = read_blif(mapped_file)


    #parse blif andpnr
    if args.hardware.startswith("chimera"):
        map_constraint = chimera_map_constraint
    else:
        map_constraint = pegasus_map_constraint

    constraints = blif_to_constraints(blif, library, map_constraint)

    for blif_const, pnr_const in zip(blif, constraints):
        print(blif_const, pnr_const.wires)

    chains, orig_graph = place_and_route(constraints, selected_hardware)

    #build final graph and output
    print(chains)
    bias_map, coupl_map, offset = build_ising(blif, chains, constraints, library, orig_graph)

    print(offset, bias_map, coupl_map)

    resp = run_annealer(args.sampler, bias_map, coupl_map, offset)

    print_runs(chains, resp)


def print_runs(chains, resp):
    for sample, energy in (resp):
        print("{}: {}".format(sample, energy))
        # print(offset + sum(sample[k] * v for k,v in bias_map.items())
        #      + sum(v * sample[i] * sample[j] for (i,j), v in coupl_map.items()))
        for wire, chain in chains.items():
            print("{}: {}".format(wire, [sample[k] for k in chain]))


def run_annealer(sampler, bias_map, coupl_map, offset):
    smp = BinaryQuadraticModel.from_ising(bias_map, coupl_map, offset)
    if sampler == "simulated":
        resp = SimulatedAnnealingSampler().sample(smp)
        resp = resp.data(["sample", "energy"])
    elif sampler == "sapi" :
        with Client.from_config() as client:

            # Load the default solver
            solver = client.get_solver()
            resp = solver.sample_ising(bias_map, coupl_map)
            resp.wait()
            resp = list(zip(resp.samples, resp.energies))
    else:
        energy = smp.energy(sampler)
        resp = [(sampler, energy)]
    return resp


def build_ising(blif, chains, constraints, library, orig_graph):
    bias_map, coupl_map, offset = defaultdict(float), defaultdict(float), 0.0
    for chain in chains.values():
        for i, j in orig_graph.subgraph(chain).edges():
            offset += 1
            coupl_map[i, j] = -1.0
            bias_map[i] += 0
            bias_map[j] += 0
    for gate, constraint in zip(blif, constraints):
        gate_name, pins = gate
        gate_data = library[gate_name]
        mapped_qubits = constraint.wires
        print(mapped_qubits, pins, gate_data)

        offset += gate_data["en0"]
        for var, bias in zip(mapped_qubits, gate_data["h"]):
            bias = bias[0]
            # spread bias evenly
            spread_bias = float(bias) / len(chains[var])
            for qubit in chains[var]:
                bias_map[qubit] += spread_bias
        for var1, coupl_list in zip(mapped_qubits, gate_data["J"]):
            for var2, coupl in zip(mapped_qubits, coupl_list):
                if abs(coupl) < 0.001: continue
                all_edges = orig_graph.edges(chains[var1])
                all_edges = list(filter(lambda e: e[1] in chains[var2], all_edges))
                try:
                    assert len(all_edges) > 0, (var1, var2)#
                except AssertionError:
                    probchains = {x:chains[x] for x in mapped_qubits}

                    print(var1,var2)
                    show_result(orig_graph, pegasus_layout(orig_graph), probchains)
                    raise
                spread_coupl = float(coupl) / len(all_edges)
                for i, j in all_edges:
                    coupl_map[i, j] += spread_coupl
    return bias_map, coupl_map, offset


def place_and_route(constraints, selected_hardware):
    hardware_size, load_hardware, expand_hardware = selected_hardware
    tile_choices, simpl_graph, orig_graph = load_hardware(hardware_size)

    placer = parheur.ParallelPlacementHeuristic(constraints, simpl_graph, tile_choices)
    thread_pool = Pool()
    placer.par_run(thread_pool, True)
    chains = expand_hardware(simpl_graph, placer.chains, orig_graph)
    return chains, orig_graph


if __name__ == '__main__':
    args = parser.parse_args()
    main(args)

def multiple_replace(string, rep_dict):
    pattern = re.compile("|".join([re.escape(k) for k in sorted(rep_dict,key=len,reverse=True)]), flags=re.DOTALL)
    return pattern.sub(lambda x: rep_dict[x.group(0)], string)

def backport_gate_data(nvars, new_gate_format):
    ret = {"en0": new_gate_format["off"]}
    totvars = nvars + int(new_gate_format["ancilla_used"])
    var_lists = ["pos{}".format(i) for i in range(1, totvars + 1)]
    ret["h"] = [[new_gate_format["(bias {})".format(i)]] for i in var_lists]
    ret["J"] = [[new_gate_format.get("(coupl {} {})".format(i, j), 0)
                 for j in var_lists]
                for i in var_lists]
    ret["area"] = totvars
    newpins = ["O", "I0", "I1", "I2", "I3", "I4", "I5"]
    oldpins = ["A", "B", "C", "D", "E", "F", "G", "H"]
    if "form" in new_gate_format:
        ret["form"] = multiple_replace(new_gate_format["form"], dict(zip(newpins, oldpins)))
    ret["dims"] = [1,1,int(ceil(float(totvars)/2))] # incorrect, todo:fix
    ret["vP"] = [[[int(pos)+1] for pos in range(totvars)]]
    return ret
