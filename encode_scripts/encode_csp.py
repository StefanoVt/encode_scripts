#!/usr/bin/env python
import logging

from encode_scripts.encode import backport_gate_data

logging.basicConfig(level=logging.INFO, format='%(levelname)s %(asctime)s %(processName)s %(message)s')

import argparse
from fractions import Fraction
from re import split, findall

from placeandroute.tilebased.chimera_tiles import load_chimera, expand_solution as chimera_expand
from placeandroute.tilebased.heuristic import Constraint
from placeandroute.tilebased.pegasus_tiles import load_pegasus, expand_solution as pegasus_expand
from pyrbc.cnf import parse_cnf as orig_parse_cnf

from .encode import place_and_route, build_ising, run_annealer, print_runs
from placeandroute.tilebased.utils import cnf_to_constraints as orig_toconstraints

def cnf_to_constraints(clauses):
    num_vars = max(max(abs(lit) for lit in clause) for _,clause in clauses)
    return orig_toconstraints(clauses, num_vars)



def bench_to_constraints(bench):
    ret = []
    for const in bench:
        name, res = const
        assert name == "TWOINFOUR", "other bench not supported yet"
        first, rest = res[0], res[1:]
        c = Constraint()
        for second in rest:
            c.add_possible_placement([[first, second], [x for x in rest if x != second]])
        c.wires = res
        ret.append(c)
    return ret


def parse_2in4(f):
    ret = []
    for line in f:
        if not line.startswith("TWOINFOUR"): continue
        res = list(map(int, split(r'[^0-9]+', line)[1:-1]))
        assert len(res) == 4, res
        ret.append(("TWOINFOUR", tuple(res)))

    return ret


def parse_cnf(f):
    cnf = orig_parse_cnf(f)
    ret = []
    constrnames = [None, None] + ["ATLEASTONEIN{}".format(x) for x in ["TWO", "THREE", "FOUR"]]
    for clause in cnf:
        assert len(clause) <= 3, "wide cnf not supported yet, soon"
        assert len(clause) > 1, "perform unit propagation first"
        print(clause)
        ret.append((constrnames[len(clause)], clause))
    return ret


hardware = {
    "chimera8": (8, load_chimera, chimera_expand),
    "chimera12": (12, load_chimera, chimera_expand),
    "chimera16": (16, load_chimera, chimera_expand),
    "pegasus6": (6, load_pegasus, pegasus_expand),
    "pegasus8": (8, load_pegasus, pegasus_expand),
    "pegasus12": (12, load_pegasus, pegasus_expand),
    "pegasus16": (16, load_pegasus, pegasus_expand),

}

# XXX todo

chimera_library = {
    "TWOINFOUR": None,
    "ATLEASTONEINTWO": None,
    "ATLEASTONEINTHREE": None,
    "ATLEASTONEINFOUR": None,
}

pegasus_library = {
    "TWOINFOUR": backport_gate_data(4,
                                    {'off': 2, '(bias pos1)': 0, '(bias pos2)': 0, '(bias pos3)': 0, '(bias pos4)': 0,
                                     '(coupl pos1 pos2)': 1, '(coupl pos1 pos3)': 1, '(coupl pos1 pos4)': 1,
                                     '(coupl pos2 pos3)': 1, '(coupl pos2 pos4)': 1, '(coupl pos3 pos4)': 1, 'pos1': 0,
                                     'pos2': 1, 'pos3': 2, 'pos4': 3, 'ancilla_used': 0}),
    "ATLEASTONEINTWO": backport_gate_data(2, {'off': Fraction(1, 2), '(bias pos1)': Fraction(-1, 2),
                                              '(bias pos2)': Fraction(-1, 2), '(bias pos3)': 0, '(bias pos4)': 0,
                                              '(coupl pos1 pos2)': Fraction(1, 2), '(coupl pos1 pos3)': 0,
                                              '(coupl pos1 pos4)': 0, '(coupl pos2 pos3)': 0, '(coupl pos2 pos4)': 0,
                                              '(coupl pos3 pos4)': 0, 'pos1': 0, 'pos2': 1, 'pos3': 2, 'pos4': 3,
                                              'ancilla_used': 0}),
    "ATLEASTONEINTHREE": backport_gate_data(3, {'off': Fraction(4000001, 4000000), '(bias pos1)': Fraction(1, 8000000),
                                                '(bias pos2)': Fraction(1, 8000000), '(bias pos3)': Fraction(-1, 2),
                                                '(bias pos4)': Fraction(-2000001, 4000000),
                                                '(coupl pos1 pos2)': Fraction(1, 2), '(coupl pos1 pos3)': 0,
                                                '(coupl pos1 pos4)': Fraction(-4000001, 8000000),
                                                '(coupl pos2 pos3)': 0,
                                                '(coupl pos2 pos4)': Fraction(-4000001, 8000000),
                                                '(coupl pos3 pos4)': Fraction(1, 2), 'pos1': 0, 'pos2': 1, 'pos3': 2,
                                                'pos4': 3, 'ancilla_used': 1}),
    # "ATLEASTONEINFOUR": backport_gate_data(4,),
}

argparser = argparse.ArgumentParser(description=
                                    """Encode CNF and CSP problems in bench form into a QA model""")

argparser.add_argument('input', help="Input problem")
argparser.add_argument('--format', default="auto", choices=["auto", "cnf", "bench"],
                       help="input problem format (default: auto)")

argparser.add_argument('--hardware', '-g', default="pegasus12", choices=hardware.keys(),
                       help="QA hardware (default: pegasus12)")
argparser.add_argument('--sampler', help="run on a sampler", default="given_solution",
                       choices=["given_solution", "simulated", "sapi"])


def main(args=None):
    if args is None:
        args = argparser.parse_args()

    if args.format == "auto":
        if args.input.endswith(".cnf"):
            args.format = "cnf"
        elif args.input.endswith(".bench"):
            args.format = "bench"
        else:
            raise ValueError("input format not recognized")

    with open(args.input) as inf:
        if args.format == "cnf":
            problem = parse_cnf(inf)
            constraints = cnf_to_constraints(problem)
        elif args.format == "bench":
            problem = parse_2in4(inf)
            constraints = bench_to_constraints(problem)
        else:
            raise ValueError("unknown problem format")

    #print(problem, constraints)

    chains, orig_graph = place_and_route(constraints, hardware[args.hardware])
    print(chains)

    bias_map, coupl_map, offset = build_ising(problem, chains, constraints, pegasus_library, orig_graph)

    if args.sampler == "given_solution":
        sol2 = dict()
        with open(args.input) as inf:
            sol = dict()
            for line in inf:
                if "solution:" not in line:
                    continue
                for match in findall(r"-?[0-9]+", line):
                    if match.startswith("-"):
                        match = match[1:]
                        val = -1
                    else:
                        val = 1
                    sol[int(match)] = val
                break
            for orig_var, val in sol.items():
                for hardware_qubit in chains[orig_var]:
                    sol2[hardware_qubit] = val
        resp = run_annealer(sol2, bias_map, coupl_map, offset)
    else:
        resp = run_annealer(args.sampler, bias_map, coupl_map, offset)

    print_runs(chains, resp)




if __name__ == '__main__':
    main()
