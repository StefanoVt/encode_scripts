#!/usr/bin/env python

import argparse
from glob import glob
from json import dumps

import networkx
from dwave_networkx.generators.chimera import chimera_graph
from gatecollector import database
from gatecollector.encodeintiles import encode_aig, describe_aig

arg_parser = argparse.ArgumentParser(description='''Build a library of pre-encoded gates''')

arg_parser.add_argument('input_dir', type=str, help="directory containing circuits to be analyzed")
arg_parser.add_argument('--output', '-o', type=argparse.FileType('w'),
                        help="GENLIB output file")
arg_parser.add_argument('--numgates', '-n', type=int,
                        help="Number of gates to get (default: all)", default=None)
arg_parser.add_argument('--cell', '-c', choices=['chimera', 'pegasus'], default='chimera',
                        help="type of cell (default: chimera tile)")


def main(args=None):
    if args is None:
        args = arg_parser.parse_args()
    db = database.DelayedFunctionDatabase()
    ngates = args.numgates
    if args.cell == 'chimera':
        graph = chimera_graph(1, 1)
    else:
        graph = networkx.complete_graph(4)
    for fn in glob(args.input_dir + "/*.aig"):
        # read all gates from file
        with open(fn, "rb") as f:
            db.read_function(f, 6)
    # save to a file the most common gates
    for i, gate in enumerate(db.most_common(ngates)):
        model = encode_aig(gate, args.cell)
        desc = describe_aig(gate)
        if model:
            jsondata = {k: float(v) for k, v in model.items()}
            print("""GATE gate{} {}.00 {};#{}
            PIN * INV 0 0 0 0 0 0 
            """.format(i,
                       model["ancilla_used"] + 1 + len(gate.inputs),
                       desc,
                       dumps(jsondata)))


if __name__ == '__main__':
    args = arg_parser.parse_args()
    main(args)