# Scripts for SATToQUBO encoding

## Notes

- `encode.py` requires having abc in the current directory. 
- `encode.py` assumes a configuration file for D-Wave Ocean SDK is present. 
  Visit the [Ocean SDK Documentation](https://docs.ocean.dwavesys.com/en/latest/index.html) 
  for further details.